//
//  ViewController.swift
//  PartnersSDKExample
//
//  Created by TestM IOS on 22/12/2019.
//  Copyright © 2019 TestM. All rights reserved.
//

import UIKit
import PartnersSDK

class PartnersExample: UIViewController {
    
    let apiKey = "14065"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PartnersSDK.setSDK(key: apiKey)
        PartnersSDK.delegate = self
        PartnersSDK.analyticsDelegate = self
    }
    
    @IBAction func start(_ sender: UIButton) {
        LoadingOverlay.shared.showOverlay(self.view, unBrandedLottie: true)
        PartnersSDK.connectToServer { (success, error) in
            LoadingOverlay.shared.hideOverlayView()
            PartnersSDK.startRun(for: .diagnostics, showSummary: true)
        }
    }
}

extension PartnersExample: TestMSDKDelegate {
    func didResolve(IMEI: String, from oldValue: String) {
        print("Did get new IMEI \(IMEI)")
    }
    
    func didStartTest(_ name: String, _ translated: String) {
        print("Did start test \(translated)")
    }
    
    func didFinishTest(_ name: String, _ translated: String, with success: Bool) {
        print("Did finish test \(translated) with \(success)")
    }
    
    func didFinishTestRun(with results: [String : Any], deviceInfo: [String : Any]) {
        print("Results: \(results)")
        print("Device Info: \(deviceInfo)")
        //Here you can dismiss the Diagnostics View controller and procceed with your custom flow or let the SDK move on to the forms view you've setup in the system dashboard
    }
}

extension PartnersExample: AnalyticsManagerDelegate {
    func didReport(with components: [AnalyticsComponents]) {
        print("SDK Analytics report of:\n\(components.compactMap({"\($0) "}))")
    }
}
